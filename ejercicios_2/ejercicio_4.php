<?php
/** Hacer un script PHP que haga lo siguiente:
* Hacer un script en PHP que haga lo siguiente:
* • El script PHP debe estar embebido en una página HTML
* • Crear una variable con el siguiente contenido 24.5
* • Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla (poner un carácter de nueva línea)
* • En la siguiente línea del script, modificar el contenido de la variable al valor “HOLA”
* • Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla (poner un carácter de nueva línea)
* • Setear el tipo de la variable que contiene “HOLA” a un tipo entero
* • Determinar con var_dump el contenido y tipo de esa variable}
*/

$variable_1 = 24.5;
$tipo_1 = gettype($variable_1);
$variable_1 = 'HOLA';
$tipo_2 = gettype($variable_1);
$variable_1 = (int)'HOLA';
var_dump($variable_1);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>
<body>
   <label><?php echo $variable_1;?></label>
   <br>
   <label><?php echo $tipo_1; ?></label>
   <br>
   <label><?php echo $variable_1;?></label>
   <br>
   <label><?php echo $tipo_2; ?></label>
   <br>
   <label><?php var_dump($variable_1); ?></label>
</body>
</html>
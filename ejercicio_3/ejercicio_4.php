<?php 
/**
* Hacer un script PHP que haga lo siguiente:
* • Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
* valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
* • Ordenar de mayor a menor los valores de estas variables.
* • Imprimir en pantalla la secuencia de números ordenadas, los números deben estar separados por espacios en blanco.
* • El número mayor debe estar en color verde y el número más pequeño debe estar en color rojo.
*/

$variable_1 = mt_rand(50, 900);
$variable_2 = mt_rand(50, 900);
$variable_3 = mt_rand(50, 900);

$array = array($variable_1, $variable_2, $variable_3);

// echo '<pre>';
// print_r($array);
// echo '</pre>';

rsort($array);

// echo '<pre>';
// print_r($array);
// echo '</pre>';

// echo $array[0];
// echo $array[0];
// echo $array[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div style="color: green"><?php echo $array[0];?></div>
    <div><?php echo $array[1];?></div>
    <div style="color: red"><?php echo $array[2];?></div>
</body>
</html>
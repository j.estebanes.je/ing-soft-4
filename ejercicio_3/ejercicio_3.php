
<?php 
/**
* Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1 y N.
* • El número N estará definido por una constante PHP. El valor de la constante N debe ser un número definido por el alumno.
* El script PHP debe estar embebido en una página HTML.
*/
$nro = 10;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Nros</th>
  </tr>
  
  <?php for ($i=0; $i < $nro; $i++) { ?>
    <?php if($i%2==0){?>
        <tr>
    <td><?php echo $i ; ?></td>
    </tr>
  <?php } ?>
  <?php } ?>
  
</table>

   
        
   
</body>
</html>



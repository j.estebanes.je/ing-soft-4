<?php
/** Hacer un script PHP que haga lo siguiente:
* • El script PHP debe estar embebido en una página HTML
* • Crear una variable que almacene su nombre y apellido.
* • Crear una variable que guarde su nacionalidad.
* • Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su
* nombre y apellido. El contenido se debe desplegar en negrita y en color rojo.
* • Usar la función print para imprimir en pantalla el contenido de la variable que almacena su
* nacionalidad. El contenido se debe desplegar subrayado.
*/

$nombre_completo = 'Juan Esteban Espinola Aquino';
$nacionalidad = 'Paraguaya';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>
<body>
    <label style='font-weight: bold; color:red'>
        <?php echo $nombre_completo ?> 
    </label>
    <br>
    <label style="text-decoration: underline;">
        <?php print ($nacionalidad) ?> 
    </label>
</body>
</html>
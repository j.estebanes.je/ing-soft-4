<?php
/** Hacer un script PHP que haga lo siguiente:
* Hacer un script PHP, utilizando la estructura de selección swicth que realice lo siguiente:
* • Se deben definir tres variables correspondientes a las notas de un alumno en un curso de PHP
* • La variable parcial1 puede tener un valor entre 0 y 30 (Se debe generar un valor aleatorio)
* • La variable parcial2 puede tener un valor entre 0 y 20 (Se debe generar un valor aleatorio)
* • La variable final1 puede tener un valor entre 0 y 50 (Se debe generar un valor aleatorio)
* Se deben sumar los tres acumulados (en la expresión del switch) e imprimir si el alumno tuvo nota 1 o 2 o 3 o 4 o 5
* Nota 1: entre 0 y 59
* Nota 2: entre 60 y 69
* Nota 3: entre 70 y 79
* Nota 4: entre 80 y 89
* Nota 5: entre 90 y 100
*/

$parcial1 = rand(0, 30);
$parcial2= rand(0, 20);
$final1= rand(0, 50);

echo $parcial1;
echo '<br>';
echo $parcial2;
echo '<br>';
echo $final1;
echo '<br>';
$suma = $parcial1+$parcial2+$final1;
echo 'suma = '.($suma);
echo '<br>';

switch ($suma) {
    case ($suma < 60):
        echo 'Nota 1';
        break;
    case ($suma >= 60 && $suma <= 69):
        echo 'Nota 2';
        break;
    case ($suma >= 70 && $suma <= 79):
        echo 'Nota 3';
        break;
    case ($suma >= 80 && $suma <= 89):
        echo 'Nota 4';
        break;
    default:
        echo 'Nota 5';
        break;
}
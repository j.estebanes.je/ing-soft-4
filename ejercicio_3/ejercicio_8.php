<?php
/**
* Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
* dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
* se debe imprimir en pantalla de manera tabular.
* Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio.
*/


$filas = 3;
$columnas = 2;


function matriz($filas, $columnas) {
    $array_filas = array();    
    for ($i=0; $i < $filas; $i++) { 
        
        $array_columnas = array();
        for ($j=0; $j < $columnas; $j++) { 
            $nro = rand(1, 99);
            array_push($array_columnas, $nro);
        }
        array_push($array_filas, $array_columnas);
    }
    
    echo '<pre>';
    print_r($array_filas);
    echo '</pre>';
}

matriz($filas, $columnas);




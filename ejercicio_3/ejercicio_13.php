<?php

/**
 * Crear un array asociativo de 10 elementos. Los valores de los 10 elemento del array deben ser strings.
 * Crear un array asociativo de un elemento. El valor del array debe ser un string.
 * El script PHP debe hacer lo siguiente:
 * • Imprimir en pantalla si existe la clave del array de un elemento en el array de 10 elementos (si no existe, se imprime un mensaje).
 * • Imprimir en pantalla si existe el valor del vector de un elemento en el vector de 10 elementos (si no existe, se imprime un mensaje).
 * • Si no existe ni la clave ni el valor (del vector de un elemento) en el vector mayor se inserta como un elemento más del vector.
 */

$array_uno = array(
    'string_1' => 'hola',
    'string_2' => 'que tal',
    'string_3' => 'como estas',
    'string_4' => 'bien',
    'string_5' => 'mal',
    'string_6' => 'comida',
    'string_7' => 'terere',
    'string_8' => 'agua',
    'string_9' => 'uwu',
    'string_10' => 'caritafachera',
);

$array_dos = array(
    'string_a' => 'caritafacheraa',
);

foreach ($array_dos as $key => $value) {
    if (array_key_exists($key, $array_uno)) {
        echo 'existe la key del array <br>';
    } else {
        echo 'no existe la key del array <br>';
    }
}

foreach ($array_dos as $key => $value) {
    if (in_array($value, $array_uno)) {
        echo 'existe el valor del array <br>';
    } else {
        echo 'no existe el valor del array <br>';
    }
}

while ($value_uno = current($array_uno)) {
    // echo key($array_uno) . '<br />';
    // echo $value_uno . '<br />';
    if (!array_key_exists(key($array_uno), $array_dos) && !in_array($value_uno, $array_dos)) {
        $array_dos[key($array_uno)] = $value_uno;
    } else {
        echo ' existe el key o valor del array <br>';
    }

    next($array_uno);
}


// echo '<pre>';
// print_r(array_keys($array_dos));
// echo '</pre>';

echo '<pre>';
print_r($array_uno);
echo '</pre>';

echo '<pre>';
print_r($array_dos);
echo '</pre>';

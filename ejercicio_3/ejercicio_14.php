<?php
// Crear un array asociativo en PHP que cumpla los siguientes requisitos:
// • Los índices/claves deben ser strings (estas cadenas deben ser generadas aleatoriamente
// por el script y deben tener una longitud de 5 a 10 caracteres).
// • Los valores deben ser números enteros del 1 a 1000.
// • Se debe imprimir el vector completo indicando índice y valor.
// • Se debe recorrer dicho array e imprimir sólo las claves del array que empiecen con la letra
// a, d, m y z (función imprimir) (en el caso se no existir ningún índice que comience con esas
// letras se debe imprimir un mensaje).
// Observación: Se debe crear un archivo por función y el archivo principal donde se llaman a las
// funciones y se realizan los procesamientos.

function superMegaGenialFuncion()
{
    $strangeAssociativeArray = array(
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
    );

    $exist = false;
    foreach ($strangeAssociativeArray as $key => $value) {
        if (
            str_starts_with($key, "a") ||
            str_starts_with($key, "d") ||
            str_starts_with($key, "m") ||
            str_starts_with($key, "z")

        ) {
            $exist = true;
            print "$key => $value \n";
        }
    }

    if ($exist == false) {
        print "no existe ningún índice que comience con esas letras";
    }
}

//generar string aleatorio 
function str_makerand($minlength = 5, $maxlength = 10, $useupper = false, $usespecial = false, $usenumbers = true)
{
    $charset = "abcdefghijklmnopqrstuvwxyz";
    if ($useupper)   $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if ($usenumbers) $charset .= "0123456789";
    if ($usespecial) $charset .= "~@#$%^*()_+-={}|][";
    if ($minlength > $maxlength) $length = mt_rand($maxlength, $minlength);
    else                         $length = mt_rand($minlength, $maxlength);
    for ($i = 0; $i < $length; $i++) $key .= $charset[(mt_rand(0, (strlen($charset) - 1)))];
    return $key;
}


superMegaGenialFuncion();

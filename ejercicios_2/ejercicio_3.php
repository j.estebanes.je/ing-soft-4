<?php
/** Hacer un script PHP que haga lo siguiente:
* El script PHP debe estar embebido en una página HTML
* • Hacer un script PHP que concatene dos cadenas con el operador punto (.) e imprimir su
* resultado. Cada cadena debe estar contenida en una variable diferente.
*/

$nombre = 'Juan Esteban';
$apellido = 'Espinola Aquino';

$nombre_completo = $nombre.' '.$apellido;

echo $nombre_completo;
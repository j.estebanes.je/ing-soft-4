<?php
/** Hacer un script PHP que haga lo siguiente:
* Hacer un script en PHP que imprima 900 números aleatorios pares
* Se deben generar números aleatorios entre 1 y 10.000
*/

for ($i=0; $i < 900; $i++) { 
    $nro = rand(1, 10000);
    if($nro%2==0){
        echo $nro . '<br>';
    } 
}
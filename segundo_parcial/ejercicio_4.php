<?php

// Crear una Base de datos llamada examen. Esta base de datos tendrá una tabla llamada  alumnos que debe tener cuatro campos: 
// id, nombre, apellido, edad (esto lo hacen de manera manual utilizando el pgAdmin o un cliente similar o creando un script de SQL) (1 punto)
// Implementar un script PHP que haga lo siguiente:
// • Inserte en la tabla alumnos creada anteriormente: 12 alumnos con valores válidos cualesquiera (1 punto)
// • Visualice de manera tabular los 12 alumnos creados en al paso previo (1 punto).
// • Crear una función que retorne el nombre y el apellido del alumno con mayor edad y el nombre y apellido del alumno con menor edad 
// (si hay edades iguales se elige uno al azar) (1 punto).

// CREATE TABLE public.alumnos (
// 	id serial NOT NULL,
// 	nombre varchar NOT NULL,
// 	apellido varchar NOT NULL,
// 	edad int NOT NULL,
// 	CONSTRAINT alumnos_pk PRIMARY KEY (id)
// );

function insertaAlumnos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=10.40.179.255;port=5434;dbname=examen", 'root', 'admin.bisendit');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Juan','Espinola','25');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Matias','Lugo','24');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Christian','Dos Santos','25');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Nelson','Aranda','24');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Homero','Thomsomp','52');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Barrack','Obama','54');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('George','Clonney','30');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Will','Smith','60');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Renato','Ferrer','22');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Osama','Laden','70');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Susana','Gimenez','60');")->execute();
        $base_de_datos->query("INSERT INTO alumnos (nombre, apellido, edad) VALUES ('Marcelo','Tinnelli','48');")->execute();
        // $select = $sql->fetchAll(PDO::FETCH_OBJ);
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
}


function listadoDeAlumnos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=10.40.179.255;port=5434;dbname=examen", 'root', 'admin.bisendit');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = $base_de_datos->query("SELECT * FROM alumnos");
        $select = $sql->fetchAll(PDO::FETCH_OBJ);

        // $result = pg_query("SELECT * FROM productos") or die('La consulta fallo: ' . pg_last_error());
        echo "<table border='1' cellpadding='5'>";

        echo "<tr>";

        echo "<td>Nombre</td>";
        echo "<td>Apellido</td>";
        echo "<td>Edad</td>";
        echo "</tr>";
        foreach ($select as $value) {
            echo "<tr>";
            echo "<td>$value->nombre</td>";
            echo "<td>$value->apellido</td>";
            echo "<td>$value->edad</td>";
            echo "</tr>";
        }

        echo "</table>";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
}
function retornoAlumno()
{
    try {
        $base_de_datos = new PDO("pgsql:host=10.40.179.255;port=5434;dbname=examen", 'root', 'admin.bisendit');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql_mayor = $base_de_datos->query("select * from alumnos where edad = (select  max(a.edad) from alumnos a);");
        $elmayor = $sql_mayor->fetch(PDO::FETCH_OBJ);

        $sql_menor = $base_de_datos->query("select * from alumnos where edad = (select  min(a.edad) from alumnos a);");
        $elmenor = $sql_menor->fetch(PDO::FETCH_OBJ);


        echo "el mayor es $elmayor->nombre $elmayor->apellido con $elmayor->edad de edad<br>";
        echo "el menor es $elmenor->nombre $elmenor->apellido con $elmenor->edad de edad";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
}


insertaAlumnos();

listadoDeAlumnos();

retornoAlumno();

<!DOCTYPE html>
<html>

<head>
  <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td,
    th,
    caption {
      border: 1px solid #dddddd;
      text-align: center;
      padding: 8px;
    }


    tr:nth-child(even) {
      background-color: #dddddd;
    }
  </style>
</head>
<title>Hola Mundo</title>

<body>

  <?php
  class Producto
  {
    public $nombre;
    public $cantidad;
    public $precio;
  }

  $productos = array();

  $productos[0] = new Producto();
  $productos[0]->nombre = "Coca Cola";
  $productos[0]->cantidad = "100";
  $productos[0]->precio = "4500";

  $productos[1] = new Producto();
  $productos[1]->nombre = "Pepsi";
  $productos[1]->cantidad = "30";
  $productos[1]->precio = "4800";

  $productos[2] = new Producto();
  $productos[2]->nombre = "Sprite";
  $productos[2]->cantidad = "20";
  $productos[2]->precio = "4500";

  $productos[3] = new Producto();
  $productos[3]->nombre = "Guarana";
  $productos[3]->cantidad = "200";
  $productos[3]->precio = "4500";

  $productos[4] = new Producto();
  $productos[4]->nombre = "SevenUp";
  $productos[4]->cantidad = "24";
  $productos[4]->precio = "4800";

  $productos[5] = new Producto();
  $productos[5]->nombre = "Mirinda Guarana";
  $productos[5]->cantidad = "89";
  $productos[5]->precio = "4800";

  $productos[6] = new Producto();
  $productos[6]->nombre = "Mirinda Naranja";
  $productos[6]->cantidad = "56";
  $productos[6]->precio = "4800";

  $productos[7] = new Producto();
  $productos[7]->nombre = "Fanta Naranja";
  $productos[7]->cantidad = "10";
  $productos[7]->precio = "4500";

  $productos[8] = new Producto();
  $productos[8]->nombre = "Fanta Pi�a";
  $productos[8]->cantidad = "2";
  $productos[8]->precio = "4500";


  // print_r($productos);
  ?>

  <table>
    <caption style='background:yellow'>Productos</caption>
    <tr>
      <th style='background:gray'>Company</th>
      <th style='background:gray'>Contact</th>
      <th style='background:gray'>Country</th>
    </tr>
    <?php foreach ($productos as $producto) { ?>
      <tr>
        <td style="text-align:left"> <?php echo ($producto->nombre); ?></td>
        <td> <?php echo ($producto->cantidad); ?></td>
        <td> <?php echo ($producto->precio); ?></td>
      </tr>

    <?php } ?>


  </table>

</body>

</html>
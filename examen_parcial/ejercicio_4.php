<?php
// 1- El script PHP debe estar incrustado en un documento HTML5 (0,5pts)
// 2- El script debe generar 5 números aleatorios NO repetidos entre el 1000 y el 7777 (1
// pto)
// 3- Se debe crear una función PHP que se le pasen los 5 números aleatorios generados y
// debe devolver el número más grande. Este número debe imprimirse en pantalla. El
// archivo php debe tener la extensión correcta. (1,5pts)


$array = array();

for ($i = 0; $i < 5; $i++) {
    $nro = rand(1000, 7777);
    // echo $nro.'<br>';

    array_push($array, $nro);
}

// print_r ($array);
// echo '<br>';

function elMayorNro($param)
{
    sort($param);
    // print_r ($param);
    // echo '<br>';
    // print_r ($param[4]);
    // echo '<br>';
    return $param[4];
}

$elMayorNro = elMayorNro($array);





?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <label>
        el nro mayor es
        <?php echo $elMayorNro; ?>
    </label>
</body>

</html>
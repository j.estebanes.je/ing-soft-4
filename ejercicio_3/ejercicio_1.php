<?php

/**
 * Hacer un script PHP que realice el siguiente cálculo
* x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x
* Donde:
* • A es la raiz cuadrada de 2
* • ¶ es el número PI
* • B es es la raíz cúbica de 3
* • C es la constante de Euler
* • D es la constante e
* Observación: Utilizar las constantes matemáticas definidas den la extensión math de PHP
 */

$A = sqrt(2);
$pi = M_PI;
$B = pow(3, 1/3);
$C = M_EULER;
$D =  exp();
$x = (($A * $pi) + $B) / ($C*$D);

print_r($x);

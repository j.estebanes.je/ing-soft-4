<?php
/** Hacer un script PHP que haga lo siguiente:
* Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
* • Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben estar entre 99 y 999). Las variables serán $a, $b y $c
* • Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la expresión $b+$c
* • Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual que la expresión $a*3
*/

$a=100;
$b=101;
$c=102;

$calculo_1 = ($a*3 > $b+$c) ? 'la expresión $a*3 es mayor que la expresión $b+$c': 'la expresión $a*3 no es mayor que la expresión $b+$c' ;
$calculo_2 =($a*3 <= $b+$c) ? 'la expresión $b+$c es mayor o igual que la expresión $a*3': 'la expresión $b+$c no es mayor o igual que la expresión $a*3';

echo $calculo_1;
echo '<br>';
echo $calculo_2;
<?php 
/**
* Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre, apellido, edad y el botón de submit.
* • Se deben usar las cadenas HEREDOC.
*/

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form style="display: flex; justify-content: center;">
        <label for="">Nombre</label>
        <input/>
        <br>
        <label for="">Apellido</label>
        <input/>
        <br>
        <label for="">Edad</label>
        <input/>
        <br>
        <button>Submit</button>
    </form>
</body>
</html>
<?php
// Implementar un script PHP que haga lo siguiente:
// • Crear una función que genere códigos de 4 letras (A-Z) aleatorias.
// • Crear un array de 500 elementos cuyo índice sea numérico y cuyo valor sean los códigos de 4 letras generados con la función anterior.
// • Imprimir ese array utilizando la estructura foreach. Se debe imprimir de manera tabular indicando índice y valor del array generado aleatoriamente.

function codigoAleatorio()
{
    $letras = "abcdefghijklmnpqrstuvwxyz";
    $tamano = strlen($letras);
    $texto = '';
    for ($i = 0; $i < 4; $i++) {
        $caracter = $letras[mt_rand(0, $tamano - 1)];
        $texto .= $caracter;
    }

    return $texto;
}

$array = array();
$a = 0;
while ($a <= 500) {
    array_push($array, codigoAleatorio());
    $a++;
}

echo "<table border='1' cellpadding='5'>";
echo "<tr>";
echo "<td>Indice</td>";
echo "<td>Valor</td>";
echo "</tr>";
foreach ($array as $key => $value) {
    echo "<tr>";
    echo "<td>$key</td>";
    echo "<td>$value</td>";
    echo "</tr>";
}

echo "</table>";

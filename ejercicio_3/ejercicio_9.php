<?php

/**
 * Realizar un script en PHP que declare un vector de 100 elementos con valores aleatorios enteros
 * entre 1 y 100 y sume todos los valores. El script debe imprimir un mensaje con la sumatoria de los
 * elementos
 * Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.
 */

$array = array();


for ($i = 0; $i < 100; $i++) {
    array_push($array, rand(1, 100));
}

$suma = array_sum($array);

echo '<pre>';
print_r($suma);
echo '</pre>';
echo '<pre>';
print_r($array);
echo '</pre>';

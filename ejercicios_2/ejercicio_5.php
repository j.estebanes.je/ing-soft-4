<?php
/** Hacer un script PHP que haga lo siguiente:
* • El script PHP debe estar embebido en una página HTML
* • Hacer un script en PHP que muestre en pantalla la tabla de multiplicar el 9, coloreando las filas alternando gris y blanco
*/
// $nro = [0,1,2,3,4,5,6,7,8,9,10];
$nro = 0;
$multiplicador = 9;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>

    <style>
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }

    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }

    tr:nth-child(even) {
    background-color: #dddddd;
    }
    </style>
</head>
<body>
<table>
  <tr>
    <th>Nro</th>
    <th>Multiplicador</th>
    <th>Resultado</th>
  </tr>
  <?php for ($i=0; $i < 11; $i++) { ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo 9;?></td>
        <th><?php echo $i*9;?></th>
    </tr>
  <?php }?>
  
  
</table>
</body>
</html>
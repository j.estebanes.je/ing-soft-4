<?php


try {
    $base_de_datos = new PDO("pgsql:host=192.168.0.14;port=5434;dbname=ejercicio1", 'root', 'admin.bisendit');
    $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = $base_de_datos->query("select 
        p.nombre as nombre_producto, 
        p.precio as precio_producto, 
        m.nombre as nombre_marca, 
        e.nombre as nombre_empresa, 
        c.nombre as nombre_categoría
    from producto p, categoria c, marca m, empresa e  
    where p.id_marca = m.id_marca 
    and p.id_categoria = c.id_categoria 
    and m.id_empresa = e.id_empresa");
    $select = $sql->fetchAll(PDO::FETCH_OBJ);
} catch (Exception $e) {
    echo "Ocurrió un error con la base de datos: " . $e->getMessage();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <table>
        <tr>
            <th>nombre_producto</th>
            <th>precio_producto</th>
            <th>nombre_marca</th>
            <th>nombre_empresa</th>
            <th>nombre_categoría</th>
        </tr>
        <?php

        foreach ($select as $row) { ?>

            <tr>
                <td><?php echo $row->nombre_producto; ?></td>
                <td><?php echo $row->precio_producto; ?></td>
                <td><?php echo $row->nombre_marca; ?></td>
                <td><?php echo $row->nombre_empresa; ?></td>
                <td><?php echo $row->nombre_categoría; ?></td>

            </tr>
        <?php  } ?>


    </table>
</body>

</html>
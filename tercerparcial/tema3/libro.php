<?php

include_once 'autor.php';

class Libro
{

    private $titulo;
    private $tipo;
    private $editorial;
    private $ano;
    private $ISBN;
    private $autor = array();

    function __construct($titulo, $tipo, $editorial, $ano, $ISBN) {
        
        $this->titulo = $titulo;
        $this->tipo = $tipo;
        $this->editorial = $editorial;
        $this->ano = $ano;
        $this->ISBN = $ISBN;
    }

    public function getNombreAutor()
    {
        $autor = $this->autor;
        foreach ($autor as $key => $a) {
            echo $a->getNombreAutor() . '<br>';    
        }
        // return $autor;
    }

    public function EscritoPor($nombre, $nacionalidad, $f_nacimiento)
    {   
        $autor = new Autor($nombre, $nacionalidad, $f_nacimiento);
        array_push($this->autor, $autor);
    }
}

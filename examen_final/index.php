<?php

$nombre = array('Alan','Antonio','Elias','Nancy','Tamara','Camila','Juan','Diego','Tomas','Alejandro','Marcos','Joaquin');
$apellido = array('Espinola','Cruz','Martinez','Lopez','Moreira','Paredes','Collante','Allanda','Rodriguez','Quintana', 'Rivero', 'Achon');


function insertaAlumnos($nombre, $apellido)
{
    try {
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // $sql = $base_de_datos->query("SELECT * FROM alumnos");
        // $select = $sql->fetchAll(PDO::FETCH_OBJ);
        $sql = "INSERT INTO alumnos (nombre, apellido, matricula) VALUES ";            

        for ($i=0; $i < 200; $i++) { 
            if($i == 199){
                $sql .= "('".$nombre[array_rand($nombre)]."','".$apellido[array_rand($apellido)]."','CO".$i."');";
            } else {
                $sql .= "('".$nombre[array_rand($nombre)]."','".$apellido[array_rand($apellido)]."','CO".$i."'),";
            }
        }
        $base_de_datos->query($sql)->execute();         
       
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}

function insertaCursos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $cursos = array('matematica', 'informatica', 'quimica', 'biologia', 'castellano', 'trigonometria', 'fisica', 'base de datos', 'aritmetica');
        $anos = array('2019', '2020', '2021', '2022', '2023', '2018', '2017', '2024', '2025');
        // $sql = $base_de_datos->query("SELECT * FROM alumnos");
        // $select = $sql->fetchAll(PDO::FETCH_OBJ);
        for ($i=0; $i < 25; $i++) { 
            for ($seccion=1; $seccion < 5; $seccion++) { 
                $sql = $base_de_datos->query("INSERT INTO cursos (nombre, ano, seccion) VALUES ('".$cursos[array_rand($cursos)]."','".$anos[array_rand($anos)]."','".$seccion."');")->execute(); 
            }
        }

        exit;
    
        
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}


function insertaInscriptos()
{
    try {
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $sql_alumnos = $base_de_datos->query("SELECT * FROM alumnos");
        $alumnos = $sql_alumnos->fetchAll(PDO::FETCH_OBJ);

        $sql_cursos = $base_de_datos->query("SELECT * FROM cursos");
        $cursos = $sql_cursos->fetchAll(PDO::FETCH_OBJ);

        
        // while ($a <= 500) {
            foreach ($alumnos as $alumno) {
                foreach ($cursos as $curso) {
                    $sql = $base_de_datos->query("INSERT INTO inscripciones (curso_id, alumno_id, fecha, activo) VALUES ('".$curso->id."', '".$alumno->id."', NOW(), true);")->execute();         
                }
                
            }
        // }
        
        
        

        exit;
    
        
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }
        
}


function cursosConMasInscriptos()
{
    try {
        // se crea la conexion
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // select de los datos
        $inscriptos = $base_de_datos->query("select count(i.alumno_id) as cant, alumno_id  , c.nombre 
        from inscripciones i 
        join cursos c on c.id = i.curso_id 
        group by i.alumno_id, c.nombre 
        order by cant asc
        limit 5;");
        $select = $inscriptos->fetchAll(PDO::FETCH_OBJ);

        //armamos la tabla
    /*--------------------------------------------------------------------------------------------*/
    echo "<table border='1' cellpadding='5'>";

        echo "<tr>";

        echo "<td>Curso</td>";
        echo "<td>Id Alumno</td>";
        
        echo "</tr>";
        foreach ($select as $value) {
            echo "<tr>";
            echo "<td>$value->nombre</td>";
            echo "<td>$value->alumno_id</td>";
            echo "</tr>";
        }
        echo "</table>";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }

}

function alumnosConMasCursos()
{
    try {
        // se crea la conexion
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // select de los datos
        $inscriptos = $base_de_datos->query("select a.nombre, a.apellido , a.matricula , count(curso_id)  as cant
        from inscripciones i 
        join cursos c on c.id = i.curso_id
        join alumnos a on a.id = i.alumno_id 
        group by a.nombre, a.apellido , a.matricula
        order by  cant asc
        limit 5;");
        $select = $inscriptos->fetchAll(PDO::FETCH_OBJ);

       
        //armamos la tabla
    /*--------------------------------------------------------------------------------------------*/
    echo "<table border='1' cellpadding='5'>";

        echo "<tr>";

        echo "<td>Alumno</td>";
        echo "<td>Matricula</td>";
        echo "<td>Cant Cursos</td>";
        
        echo "</tr>";
        foreach ($select as $value) {
            echo "<tr>";
            echo "<td>$value->nombre $value->apellido</td>";
            echo "<td>$value->matricula</td>";
            echo "<td>$value->cant</td>";
            echo "</tr>";
        }
        echo "</table>";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }

}

function fechasConMasInscripciones()
{
    try {
        // se crea la conexion
        $base_de_datos = new PDO("pgsql:host=192.168.0.13;port=5434;dbname=finalSegunda", 'root', 'exalumnos');
        $base_de_datos->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // select de los datos
        $inscriptos = $base_de_datos->query("select fecha , count(alumno_id)  as cant
        from inscripciones i 
        join cursos c on c.id = i.curso_id
        join alumnos a on a.id = i.alumno_id 
        group by fecha
        order by  cant asc
        limit 5;");
        $select = $inscriptos->fetchAll(PDO::FETCH_OBJ);

       
        //armamos la tabla
    /*--------------------------------------------------------------------------------------------*/
    echo "<table border='1' cellpadding='5'>";

        echo "<tr>";

        echo "<td>Fecha</td>";
        echo "<td>Cant Inscriptos</td>";
        
        echo "</tr>";
        foreach ($select as $value) {
            echo "<tr>";
            echo "<td>$value->fecha</td>";
            echo "<td>$value->cant</td>";
            echo "</tr>";
        }
        echo "</table>";
    } catch (Exception $e) {
        echo "Ocurrió un error con la base de datos: " . $e->getMessage();
    }

}


// se debe descomentar si se desea realizar los insert
// insertaAlumnos($nombre, $apellido);
// insertaCursos();
// insertaInscriptos();
cursosConMasInscriptos();
alumnosConMasCursos();
fechasConMasInscripciones();

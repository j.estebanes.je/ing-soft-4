<?php

/**
 * Hacer un script en PHP que se conecte a la BD, anteriormente creada en el “ejercicio1” y haga lo siguiente:
 * o Realizar una consulta a dicha BD para que traiga los siguientes datos (respetar orden):
 * ▪ nombre_producto,precio_producto,nombre_marca, nombre_empresa,nombre_categoría
 * o Presentar los datos de manera tabular (usando tablas HTML generadas a partir de PHP)
 */



$conexion = pg_connect('host=192.168.0.14 port=5434 dbname=ejercicio1 user=root password=admin.bisendit');

if ($conexion) {
    $sql = pg_exec($conexion, "select p.nombre, p.precio, m.nombre, e.nombre , c.nombre  
    from producto p, categoria c, marca m, empresa e  
    where p.id_marca = m.id_marca 
    and p.id_categoria = c.id_categoria 
    and m.id_empresa = e.id_empresa");
} else {
    echo 'Error a conectar la bd';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>

<body>
    <table>
        <tr>
            <th>nombre_producto</th>
            <th>precio_producto</th>
            <th>nombre_marca</th>
            <th>nombre_empresa</th>
            <th>nombre_categoría</th>
        </tr>
        <?php

        while ($row = pg_fetch_row($sql)) { ?>

            <tr>
                <td><?php echo $row[0]; ?></td>
                <td><?php echo $row[1]; ?></td>
                <td><?php echo $row[2]; ?></td>
                <td><?php echo $row[3]; ?></td>
                <td><?php echo $row[4]; ?></td>
            </tr>
        <?php  } ?>


    </table>
</body>

</html>

<?php
pg_close();
?>
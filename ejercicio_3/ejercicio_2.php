<?php
/* Hacer un script PHP que imprime la siguiente información:
* • Versión de PHP utilizada.
* • El id de la versión de PHP.
* • El valor máximo soportado para enteros para esa versión.
* • Tamaño máximo del nombre de un archivo.
* • Versión del Sistema Operativo.
* • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
* • El include path por defecto.
*/

$a = PHP_VERSION;
$b = PHP_VERSION_ID;
$c = PHP_INT_MAX;
$d = filesize();
$e = php_uname();
$f = PHP_EOL;
$g = set_include_path();

print_r($a);
echo '<br>';
print_r($b);
echo '<br>';
print_r($c);
echo '<br>';
print_r($d);
echo '<br>';
print_r($e);
echo '<br>';
print_r($f);
echo '<br>';
print_r($g);